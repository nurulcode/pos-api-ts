# PosApi

This is a sample application built with TypeScript and MongoDB Atlas.
## Installation
1.  **Clone the Repository:**
```bash

https://gitlab.com/nurulcode/pos-api-ts.git

```

2.  **Install Dependencies:**

Navigate to the project directory and install the dependencies using npm:

```bash
cd pos-api-ts && npm install

```

3.  **Set up MongoDB Atlas:**

- Sign up or log in to your [MongoDB Atlas](https://www.mongodb.com/cloud/atlas) account.
- Create a new cluster.
- Get your connection string.
- Update the connection string in your application's configuration (e.g., `src/config.ts`).

## Running the Application

1.  **Build TypeScript Code:**

Compile the TypeScript code into JavaScript:

```bash
npm start

```

2.  **Start the Server:**

Start the application server:

```bash
npm run dev

```

3.  **Access the Application:**

The application should now be running on `http://localhost:3000`. You can access it through your web browser or any HTTP client.

## Development Mode

- During development, you can run the server in watch mode to automatically restart on file changes:

```bash
npm run dev

```

- You can also compile TypeScript files manually:

```bash
npm run build

```

## Environment Variables

- Make sure to set up environment variables for sensitive information such as database credentials. You can use a `.env` file for local development and environment variables in production.
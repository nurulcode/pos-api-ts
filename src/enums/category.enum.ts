enum CategoryType {
    Car = 'Car',
    Dress = 'Dress',
    Pants = 'Pants',
}

export default CategoryType
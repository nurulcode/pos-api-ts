import { Router } from "express";
import { userAuth, userGetAll, userGetById, userRegister } from "../controllers/user.controller";
import { isAdmin, protect } from "../middleware/auth.middleware";
const router = Router()

router.route('/')
    .post(userRegister)
    .get(protect, isAdmin, userGetAll)

router.route('/:id')
    .get(protect, userGetById)

router.route('/login')
    .post(userAuth)

export default router;
import express from 'express';
import connectDB from './config/db.config';
import dotenv from 'dotenv'

import userRoutes from './routers/user.routes'
import morgan from 'morgan';
import bodyParser from 'body-parser';
dotenv.config();

const app = express();
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

if (process.env.NODE_ENV == 'development') {
  app.use(morgan('dev'))
}

const port = process.env.PORT || 3000;

app.use('/api/users', userRoutes)

// connecting to Mongodb and starting the server
const startDB = async (): Promise<void> => {
  try {
    await connectDB(process.env.MONGO_URI);
    console.log('Mongodb is connected!!!')
    app.listen(port, () => {
      console.log(`Server is listening on port ${port}...`);
    })
  } catch (error: any) {
    console.log(error);
  }
}
startDB();

import { Request, Response } from 'express';
import User from '../models/user.model';
import { generateToken, generateRefreshToken } from '../utils/generateToken.util';
import { ErrorHandler } from '../utils/errorHandler.util';
import { body, param, validationResult } from 'express-validator';

const userAuth = async (req: Request, res: Response) => {
    await body('email').notEmpty().run(req);
    await body('password').notEmpty().run(req);

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({
            status: false,
            message: 'Validation failed',
            errors: errors.array()
        });
    }

    const { email, password } = req.body;

    try {
        const user = await User.findOne({ email })

        if (user && (await user.verifyPassword(password))) {
            res.status(200).json({
                _id: user.id,
                name: user.username,
                email: user.email,
                token: generateToken(user._id),
                refreshToken: generateRefreshToken(user._id)
            })
        }
    } catch (error: any) {
        ErrorHandler(500, 'Internal Server Error', res)
    }
}

const userRegister = async (req: Request, res: Response): Promise<void> => {
    await body('email').isEmail().normalizeEmail().run(req);
    await body('username').notEmpty().run(req);
    await body('password').notEmpty().isLength({ min: 6 }).run(req);

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({
            status: false,
            message: 'Validation failed',
            errors: errors.array()
        });
    }

    const { username, email, password } = req.body

    try {
        const exist = await User.findOne({ email: email })
        if (exist) {
            res.status(400).json({ message: 'User already exist!' })
            return;
        }

        const user = await User.create({ username, email, password })
        if (user) {
            res.status(201).json({
                status: 'success',
                message: 'Insert data successfully',
                data: {
                    _id: user.id,
                    username: user.username,
                    email: user.email,
                }

            });
        } else {
            res.status(400).json({ message: 'Invalid user data' });
        }


    } catch (error: any) {
        res.status(500).json({ message: error.message || 'Internal Server Error' });
    }
}

const userGetById = async (req: Request, res: Response): Promise<void> => {
    try {
        const userc = await User.findById(req.params.id).select('-password')
        if (userc) {
            res.json(userc);
        } else {
            ErrorHandler(404, 'User not found', res)
        }

    } catch (error: any) {
        ErrorHandler(500, 'Internal Server Error', res)
    }
}

const userGetAll = async (req: Request, res: Response): Promise<void> => {
    try {
        const userc = await User.find({}).select(['-password'])
        res.json(userc);
    } catch (error: any) {
        ErrorHandler(500, 'Internal Server Error', res)
    }
}

const userUpdate = async (req: Request, res: Response): Promise<void> => {
    await param('id').notEmpty().run(req);

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({
            status: false,
            message: 'Validation failed',
            errors: errors.array()
        });
    }

    try {
        const userc = await User.findById(req.params.id)
        if (!userc) {
            ErrorHandler(404, 'User not found', res)
        }

        res.status(200).json({
            status: true,
            message: 'Update data succesfully',
            data: {
                _id: userc?._id,
                username: userc?.username,
                email: userc?.email,
                isAdmin: userc?.isAdmin
            }
        });
    } catch (error: any) {
        ErrorHandler(500, 'Internal Server Error', res)
    }
}

const userDelete = async (req: Request, res: Response): Promise<void> => {
    await param('id').notEmpty().run(req);

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({
            status: false,
            message: 'Validation failed',
            errors: errors.array()
        });
    }

    try {
        const userc = await User.findById(req.params.id)
        if (!userc) {
            ErrorHandler(404, 'User not found', res)
        } else {
            userc.username = req.body?.username || userc.username;
            userc.email = req.body?.email || userc.email;
            if (req.body?.password) {
                userc.password = req.body?.password;
            }

            const update = await userc.save();

            res.status(200).json({
                status: true,
                message: 'Update data succesfully',
                data: {
                    _id: update?._id,
                    username: update?.username,
                    email: update?.email,
                    isAdmin: update?.isAdmin,
                    token: generateToken(update?._id),
                }
            });
        }
    } catch (error: any) {
        ErrorHandler(500, 'Internal Server Error', res)
    }
}

const userGetProfile = async (req: Request, res: Response): Promise<void> => {
    try {
        const userc = await User.findById(req.user._id)
        if (!userc) {
            ErrorHandler(404, 'User not found', res)
        }

        res.status(200).json({
            status: true,
            message: 'Update data succesfully',
            data: {
                _id: userc?._id,
                username: userc?.username,
                email: userc?.email,
                isAdmin: userc?.isAdmin
            }
        });
    } catch (error: any) {
        ErrorHandler(500, 'Internal Server Error', res)
    }
}

const userUpdateProfile = async (req: Request, res: Response): Promise<void> => {
    try {
        const userc = await User.findById(req.user._id)
        if (!userc) {
            ErrorHandler(404, 'User not found', res)
        } else {
            userc.username = req.body?.username || userc.username;
            userc.email = req.body?.email || userc.email;
            if (req.body?.password) {
                userc.password = req.body?.password;
            }

            const update = await userc.save();

            res.status(200).json({
                status: true,
                message: 'Update data succesfully',
                data: {
                    _id: update?._id,
                    username: update?.username,
                    email: update?.email,
                    isAdmin: update?.isAdmin,
                    token: generateToken(update?._id),
                }
            });
        }
    } catch (error: any) {
        ErrorHandler(500, 'Internal Server Error', res)
    }
}

export {
    userRegister,
    userGetById,
    userGetAll,
    userAuth,
    userGetProfile,
    userUpdateProfile,
    userDelete,
    userUpdate,
}
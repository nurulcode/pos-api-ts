import mongoose, { Schema, Document } from "mongoose";
import CategoryType from "../enums/category.enum";

interface IProduct extends Document {
    product_name: string,
    price: string,
    category: CategoryType,
}

const productSchema: Schema<IProduct> = new Schema({
    product_name: {
        type: String,
        required: true
    },
    price: {
        type: String,
        required: true,
        unique: true
    },
    category: {
        type: String,
        enum: Object.values(CategoryType)
    }
}, {
    timestamps: true
})


const Product = mongoose.model<IProduct>('Product', productSchema);

export default Product;
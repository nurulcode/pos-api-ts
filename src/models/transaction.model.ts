import mongoose, { Schema, Document } from "mongoose";
import CategoryType from "../enums/category.enum";

interface ITransaction extends Document {
    product_id: mongoose.Types.ObjectId,
    quantity: number,
    total_price: number,
    transaction_date: Date,
    user_id: mongoose.Types.ObjectId,
}

const transactionSchema: Schema = new Schema({
    product_id: {
        type: Schema.Types.ObjectId,
        ref: 'Product',
        required: true
    },
    quantity: {
        type: String,
        required: true,
    },
    total_price: {
        type: Number,
        required: true,
    },
    transaction_date: {
        type: Number,
        required: true,
    },
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
}, {
    timestamps: true
})


const Transaction = mongoose.model<ITransaction>('Transaction', transactionSchema);

export default Transaction;
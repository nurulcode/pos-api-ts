import mongoose, { Schema } from "mongoose";
import bcrypt from "bcryptjs";
import { IUser } from "../interfaces/user.interface";


const userSchema: Schema<IUser> = new Schema({
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
    },
    isAdmin: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
})

userSchema.pre<IUser>('save', async function (next) {
    const user = this;

    if (!user.isModified('password')) return next()
    const saltRounds = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(user.password, saltRounds)
    user.password = hashedPassword;
    next()
})

userSchema.methods = {
    verifyPassword: async function (password: string): Promise<boolean> {
        return bcrypt.compare(password, this.password)
    }
}

const User = mongoose.model<IUser>('User', userSchema);

export default User;
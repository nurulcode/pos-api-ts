import { JwtPayload, Secret, sign } from "jsonwebtoken"
import dotenv from "dotenv";
dotenv.config();

const generateToken = (id: string): string | JwtPayload => {
    const secretKey : Secret = process.env.JWT_SECRET ? process.env.JWT_SECRET : 'secretKey';
    return sign({ id }, secretKey, {
        expiresIn: '30d'
    })
}

const generateRefreshToken = (id: string): string | JwtPayload => {
    const secretKeyRefresh : Secret = process.env.JWT_SECRET_REFRESH ? process.env.JWT_SECRET_REFRESH : 'secretKeyRefresh';
    return sign({ id }, secretKeyRefresh, {
        expiresIn: '30d'
    })
}

export {
    generateToken,
    generateRefreshToken
}
import { Response } from "express";

const ErrorHandler = (statusCode : number, message: string, res : Response) =>  {
    res.status(statusCode).json({
        status : false,
        message : message
    })
}

export {
    ErrorHandler
};
import { NextFunction, Request, Response } from "express";
import { JwtPayload, Secret, verify } from "jsonwebtoken";
import dotenv from "dotenv";
import User from "../models/user.model";
import { IUser } from "../interfaces/user.interface";
import { ErrorHandler } from "../utils/errorHandler.util";
dotenv.config()

declare global {
    namespace Express {
        interface Request {
            user?: any
        }
    }
}

const protect = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    let token = req.headers.authorization;

    if (token && token.startsWith('Bearer')) {

        try {
            token = token.split(" ")[1];
            const secret: Secret = process.env.JWT_SECRET || '';
            const decoded = verify(token, secret)

            if (typeof decoded === 'string') {
                res.status(401);
                ErrorHandler(401, 'Invalid token', res)
            }

            const id = (decoded as JwtPayload).id;

            const user = await User.findById(id).select('-password')
            if (!user) {
                res.status(401);
                ErrorHandler(401, 'User not found', res)

            }

            req.user = user;
            next();
        } catch (error: any) {
            console.error(error);
            ErrorHandler(401, 'Not authorized, token failed', res)
        }

    }

    if (!token) {
        ErrorHandler(401, 'Not authorized, no token', res)
    }
}

const isAdmin = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    if (!req.user.isAdmin) {
        ErrorHandler(401, 'Not authorized as an admin"', res)
    }
    next()
}

export {
    protect,
    isAdmin
}